function replaceTextRecursively(element, from, to) {
    if (element.childNodes.length)
    {
        element.childNodes.forEach(child => replaceTextRecursively(child, from, to));
    }
    else {
        const cont = element.textContent;
        if (cont) element.textContent = cont.replace(from, to);
    }
};

replaceTextRecursively(document.body, new RegExp("r", "g"), "R")
replaceTextRecursively(document.body, new RegExp("g", "g"), "⅁")
replaceTextRecursively(document.body, new RegExp("G", "g"), "⅁")
